<!DOCTYPE html>
<html lang="<?php echo Theme::lang() ?>">
<head>
<?php include(THEME_DIR_PHP.'head.php'); ?>
</head>
<body>
	<?php include(THEME_DIR_PHP.'icons.php'); ?>
	<!-- Load Bludit Plugins: Site Body Begin -->
	<?php Theme::plugins('siteBodyBegin'); ?>

	<div id="wrapper">
		<!-- Navbar -->
		<?php include(THEME_DIR_PHP.'header.php'); ?>

		<div class="container-blog">
			<?php
				// Bludit content are pages
				// But if you order the content by date
				// These pages works as posts

				// $WHERE_AM_I variable detect where the user is browsing
				// If the user is watching a particular page/post the variable takes the value "page"
				// If the user is watching the frontpage the variable takes the value "home"
				if ($WHERE_AM_I == 'page') {
					include(THEME_DIR_PHP.'page.php');
				} elseif ($WHERE_AM_I == 'category') { 
					include(THEME_DIR_PHP.'category.php');
				} elseif ($WHERE_AM_I == 'tag') { 
					include(THEME_DIR_PHP.'tag.php');
				} elseif ($WHERE_AM_I == 'search') { 
					include(THEME_DIR_PHP.'search.php');
				} else {
					include(THEME_DIR_PHP.'home.php');
				}
			?>
		</div>

	</div>

	<!-- Footer -->
	<?php include(THEME_DIR_PHP.'footer.php'); ?>

	<!-- Load Bludit Plugins: Site Body End -->
	<?php Theme::plugins('siteBodyEnd'); ?>
</body>
</html>