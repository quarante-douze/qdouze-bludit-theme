<div class="bg-dark skip small-text fg-light">
	<a href="#skip">Accéder au contenu</a>
</div>
<header id="page-header">
	<div class="osd">
		<div class="container menu toolbar fg-light">
			<nav class="d-none d-flex-sm">
				<h2 class="sr-only">Menu des pages</h2>
				<ul>
					<li>
						<a href="<?php echo Theme::siteUrl() ?>" class="menu-item">
							<svg class="icon icon-home" alt=""><use xlink:href="#icon-home"></use></svg><span class="sr-only">Accueil</span>
						</a>
					</li>
					<li>
						<a href="#" class="menu-item submenu">
							Pages <svg class="icon icon-caret-down" alt=""><use xlink:href="#icon-caret-down"></use></svg>
						</a>
						<ul class="bg menu fg-dark">
							<?php foreach ($staticContent as $tmp) : ?>
								<?php if ($tmp->position() >= 10) : ?>
									<li><a class="mr-3 ml-3" href="<?php echo $tmp->permalink(); ?>"><?php echo $tmp->title(); ?></a></li>
								<?php endif ?>
							<?php endforeach ?>
						</ul>
					</li>
					<?php foreach ($staticContent as $tmp) : ?>
						<?php if ($tmp->position() < 10) : ?>
							<li><a href="<?php echo $tmp->permalink(); ?>"><?php echo $tmp->title(); ?></a></li>
						<?php endif ?>
					<?php endforeach ?>
				</ul>
			</nav>

			<ul class="f-end d-flex">
				<li>
					<div>
							<label for="searchfield" class="sr-only">Formulaire de recherche</label>
							<input class="form-control" id="search-input" type="search" placeholder="Rechercher sur le blog" aria-label="Rechercher sur le blog" value="" name="s" id="s" />
						</div>
				</li>
				<script>
						function searchNow() {
							var searchURL = "<?php echo Theme::siteUrl(); ?>search/";
							window.open(searchURL + document.getElementById("search-input").value, "_self");
						}
						document.getElementById("search-input").onkeypress = function(e) {
							if (!e) e = window.event;
							var keyCode = e.keyCode || e.which;
							if (keyCode == '13' && document.getElementById("search-input").value !== "") {
								searchNow();
								return false;
							}
						}
					</script>
			</ul>
		</div>
	</div>

	<div class="logo-area">
    <h1>
			<a href="<?php echo Theme::siteUrl() ?>"><img src="<?php echo HTML_PATH_THEME_IMG; ?>/logo.png" alt=""/> 
				<span class="sr-only"><?php echo $site->title() ?></span>
			</a>
		</h1>
	</div>
	<nav class="container menu toolbar bg fg-dark categories d-none d-flex-sm">
		<h2 class="sr-only">Liste des catégories</h2>
		<ul class="f-around">
			<?php
				$items = getCategories();
				foreach ($items as $category) {
					if (count($category->pages())>0) {
					?>
					<li><a href="<?php echo $category->permalink(); ?>"><?php echo  $category->name(); ?> </a></li>
				<?php }
				}
			?>
		</ul>
	</nav>
</header>
