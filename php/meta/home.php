<meta property="og:type" content="website" />
<meta property="og:url" content="https://quarante-douze.net/" />
<meta property="og:title" content="<?php echo $site->title() ?>" />
<meta property="og:description" content="<?php echo $site->description() ?>" />
<meta property="og:image" content="<?php echo Theme::siteUrl() ?><?php echo HTML_PATH_THEME_IMG; ?>/default-preview.png" />