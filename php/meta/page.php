<?php Theme::plugins('pageBegin'); ?>
<meta property="og:type" content="article" />
<meta property="og:url" content="<?php echo $page->permalink(); ?>" />
<meta property="og:title" content="<?php echo $page->title(); ?> | <?php echo $site->title() ?>" />
<meta property="og:description" content="<?php echo $page->description(); ?>" />
<?php if ($page->coverImage()) : ?>
    <meta property="og:image" content="<?php echo $page->coverImage(); ?>" />
<?php else : ?>
    <meta property="og:image" content="<?php echo Theme::siteUrl() ?><?php echo HTML_PATH_THEME_IMG; ?>/default-preview.png" />
<?php endif ?>
<meta property="article:published_time" content="<?php echo $page->date(DATE_ATOM) ?>" />
<meta property="article:author:username" content="<?php echo $page->user('nickname'); ?>" />
<!-- TODO: rendre ça paramétrable -->
<meta property="fediverse:author" name="fediverse:author" content="kazhnuz@toot.kobold.cafe" />
<?php Theme::plugins('pageEnd'); ?>