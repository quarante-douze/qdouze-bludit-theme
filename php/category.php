<main class="h-feed">
  <h1 class="page-title p-name" id="title-featured">
    <svg class="icon" alt=""><use xlink:href="#icon-folder"></use></svg> 
    <?php 
      $category = new Category($url->slug());
      echo $category->name();
    ?>
  </h1>
  <div class="previews-section preview-list">
      <?php foreach ($content as $page) : ?>
      <?php if ($page->type() != "sticky") : ?>
        <div class="h-entry">
          <a href="<?php echo $page->permalink(); ?>" class="preview u-url">
            <div class="preview-thumbnail">
              <?php if ($page->thumbCoverImage()) : ?>
                <img alt="" src="<?php echo $page->thumbCoverImage(); ?>" class="u-photo" />  
              <?php else : ?>
                <img alt="" src="<?php echo HTML_PATH_THEME_IMG; ?>/default-preview-small.png" class="u-photo" />
              <?php endif ?>
            </div>
            <div class="preview-text">
              <h2 class="p-name"><?php echo $page->title(); ?></h2>
            
              <div class="flex-that">
                <div>
                  <span class='badge c-secondary small-text m-0 p-category'><?php echo $page->category(); ?></span>
                </div>
                <div>
                  <time class="dt-published" datetime="<?php echo $page->date(DATE_ATOM) ?>" ><span class="badge c-secondary small-text m-0"><?php echo $page->date('d/m/Y') ?></span></time>
                </div>
              </div>
              <div class="preview-excerpt p-summary"><?php echo $page->description(); ?></div>
            </div>
          </a>
        </div>
      <?php endif ?>
      <?php endforeach ?>
    </div>

  <?php if (Paginator::numberOfPages() > 1) : ?>
    <nav class="paginator mb-2 mt-1">
      <ul class="pagination flex-that no-pills">

        <!-- Previous button -->
        <?php if (Paginator::showPrev()) : ?>
          <li class="page-item m-0 p-0">
            <a class="page-link btn btn-primary m-0" href="<?php echo Paginator::previousPageUrl() ?>" tabindex="-1"><?php echo $L->get('Previous'); ?></a>
          </li>
        <?php else : ?>
          <li class="page-item m-0 p-0">
            <span class="page-link m-0" tabindex="-1"><?php echo $L->get('Previous'); ?></span>
          </li>
        <?php endif; ?>

        <!-- Home button -->
        <li class="page-item p-0 m-0">
          <span>Page <?php echo Paginator::currentPage(); ?> sur <?php echo Paginator::numberOfPages(); ?> </span>
        </li>

        <!-- Next button -->
        <?php if (Paginator::showNext()) : ?>
          <li class="page-item m-0 p-0">
            <a class="page-link btn btn-primary m-0" href="<?php echo Paginator::nextPageUrl() ?>"><?php echo $L->get('Next'); ?></a>
          </li>
        <?php else : ?>
          <li class="page-item m-0 p-0">
            <span class="page-link m-0" tabindex="-1"><?php echo $L->get('Next'); ?> </span>
          </li>
        <?php endif; ?>

      </ul>
    </nav>
  <?php endif ?>
</main>

<?php include(THEME_DIR_PHP.'sidebar.php'); ?>
