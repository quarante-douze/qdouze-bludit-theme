<footer class="bg-dark fg-light" aria-labelledby="title-footer">
  <h1 class="sr-only" id="title-footer">Informations sur le site</h1>
  <nav class="container menu toolbar bg fg-dark" aria-label="title-footer-social">
    <h2 class="sr-only" id="title-footer-social">Me suivre</h2>
    <ul class="f-end">
				<!-- Social Networks -->
				<?php foreach (Theme::socialNetworks() as $key=>$label): ?>
				<li>
					<a href="<?php echo $site->{$key}(); ?>" class="menu-item">
						<svg class="icon" alt=""><use xlink:href="#icon-<?php echo $key ?>"></use></svg>
						<span class="sr-only"><?php echo $label; ?></span>
					</a>
				</li>
				<?php endforeach; ?>
      <li>
				<a href="/rss.xml" class="menu-item">
					<svg class="icon" alt=""><use xlink:href="#icon-rss"></use></svg>
					<span class="sr-only">Flux RSS du site</span>
				</a>
			</li>
    </ul>
  </nav>
  <div class="container columns">
    <section class="col-12 col-md-4">
      <p>Les contenus sont diffusé sous licence <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr" rel="licence">Creative Common Attribution - Partage à l'Identique 4.0</a> hors mention contraire.</p>
      <p>Ces licences vous autorise à partager et copier mes travaux, tant que vous me citiez en source, et que vous autorisez la même chose pour les travaux qui en seraient dérivés. N'hésitez pas à partager ! ❤️</p>
    </section>

    <section class="col-12 col-md-4">
      <h2 class="sr-only" id="title-footer-section-2">Crédits</h2>
      <p>Ce site est propulsé par <a target="_blank" class="text-white" href="https://www.bludit.com"><?php echo (defined('BLUDIT_PRO'))?'Bludit Pro':'Bludit' ?></span></a></p>
      <p>Le <a href="https://git.kobold.cafe/quarante-douze/qdouze2-wordpress-theme">theme</a> de ce blog est disponible sous licence CC BY-SA et GPL v3. Il utilise un framework custom, des icones venant de <a href="https://forkaweso.me/Fork-Awesome/">Fork-Awesome</a>, et la palette <a href="https://yeun.github.io/open-color/">Open-Color</a></p>
    </section>

    <section class="col-12 col-md-4">
      <p>Ce site n'a pas de système de commentaire intégré, pour éviter le potentiel travail de modération, gestion des spams, etc (que je préfère faire sur des espaces communautaires).</p> 
      <p>Cependant toute critique, remarque, etc. est la bienvenue. Pour cela, vous pouvez me contacter à kazhnuz [at] kobold [point] cafe ou sur mes <a href="https://kazhnuz.space/links">réseaux sociaux</a>.</p>
    </section>
  </div>
</footer>