<main id="skip" class="fullwidth">
  <?php Theme::plugins('pageBegin'); ?>
  <?php if (Paginator::currentPage() == 1) : ?>
    <section aria-labelledby="title-featured" class="h-feed">
      <h1 class="page-title" id="title-featured">
        <svg class="icon icon-star" viewBox="0 0 30 32"><path d="M29.714 11.554c0 0.321-0.232 0.625-0.464 0.857l-6.482 6.321 1.536 8.929c0.018 0.125 0.018 0.232 0.018 0.357 0 0.464-0.214 0.893-0.732 0.893-0.25 0-0.5-0.089-0.714-0.214l-8.018-4.214-8.018 4.214c-0.232 0.125-0.464 0.214-0.714 0.214-0.518 0-0.75-0.429-0.75-0.893 0-0.125 0.018-0.232 0.036-0.357l1.536-8.929-6.5-6.321c-0.214-0.232-0.446-0.536-0.446-0.857 0-0.536 0.554-0.75 1-0.821l8.964-1.304 4.018-8.125c0.161-0.339 0.464-0.732 0.875-0.732s0.714 0.393 0.875 0.732l4.018 8.125 8.964 1.304c0.429 0.071 1 0.286 1 0.821z"></path></svg> 
        <span class="p-name">À la une</span>
      </h1>
      <div id="featured-articles">
        <?php foreach ($content as $page) : ?>
        <?php if ($page->type() == "sticky") : ?>
          <article class="preview-featured h-entry">
            <a href="<?php echo $page->permalink(); ?>" class="preview-link u-url">
              <img src='<?php echo ($page->thumbCoverImage() ? $page->thumbCoverImage() : HTML_PATH_THEME_IMG . "/default-preview-small.png") ?>' class="d-none u-photo">
              <div class="preview-item" style="background-image:url('<?php echo ($page->thumbCoverImage() ? $page->thumbCoverImage() : HTML_PATH_THEME_IMG . "/default-preview.png") ?>');">
                <div class="preview-overlay">
                  <div class="preview-categories">
                    <span class='badge c-secondary p-category'><?php echo $page->category(); ?></span>
                  </div>
                  <h2 class="preview-title p-name"><?php echo $page->title(); ?></h2>
                </div>
              </div>
            </a>
          </article>
        <?php endif ?>
        <?php endforeach ?>
      </div>
    </section>
  <?php endif ?>
  <section aria-labelledby="title-publications" class="h-feed">
    <h1 class="page-title" id="title-publications"><svg class="icon icon-newspaper-o" alt="" viewBox="0 0 37 30"><path d="M18.286 9.143h-6.857v6.857h6.857v-6.857zM20.571 20.571v2.286h-11.429v-2.286h11.429zM20.571 6.857v11.429h-11.429v-11.429h11.429zM32 20.571v2.286h-9.143v-2.286h9.143zM32 16v2.286h-9.143v-2.286h9.143zM32 11.429v2.286h-9.143v-2.286h9.143zM32 6.857v2.286h-9.143v-2.286h9.143zM4.571 24v-17.143h-2.286v17.143c0 0.625 0.518 1.143 1.143 1.143s1.143-0.518 1.143-1.143zM34.286 24v-19.429h-27.429v19.429c0 0.393-0.071 0.786-0.196 1.143h26.482c0.625 0 1.143-0.518 1.143-1.143zM36.571 2.286v21.714c0 1.893-1.536 3.429-3.429 3.429h-29.714c-1.893 0-3.429-1.536-3.429-3.429v-19.429h4.571v-2.286h32z"></path></svg> <span class="p-name">Derniers articles</span></h1>

    <div class="previews-section preview-grid">
      <?php foreach ($content as $page) : ?>
      <?php if ($page->type() != "sticky") : ?>
        <div class="h-entry">
          <a href="<?php echo $page->permalink(); ?>" class="preview u-url">
            <div class="preview-thumbnail">
              <?php if ($page->thumbCoverImage()) : ?>
                <img alt="" src="<?php echo $page->thumbCoverImage(); ?>" class="u-photo" />  
              <?php else : ?>
                <img alt="" src="<?php echo HTML_PATH_THEME_IMG; ?>/default-preview-small.png" class="u-photo" />
              <?php endif ?>
            </div>
            <div class="preview-text">
              <h2 class="p-name"><?php echo $page->title(); ?></h2>
            
              <div class="flex-that">
                <div>
                  <span class='badge c-secondary small-text m-0 p-category'><?php echo $page->category(); ?></span>
                </div>
                <div>
                  <time class="dt-published" datetime="<?php echo $page->date(DATE_ATOM) ?>" ><span class="badge c-secondary small-text m-0"><?php echo $page->date('d/m/Y') ?></span></time>
                </div>
              </div>
              <div class="preview-excerpt p-summary"><?php echo $page->description(); ?></div>
            </div>
          </a>
        </div>
      <?php endif ?>
      <?php endforeach ?>
    </div>

<!-- Pagination -->
<?php if (Paginator::numberOfPages() > 1) : ?>
  <nav class="paginator mb-2 mt-1">
    <ul class="pagination flex-that no-pills">

      <!-- Previous button -->
      <?php if (Paginator::showPrev()) : ?>
        <li class="page-item m-0 p-0">
          <a class="page-link btn btn-primary m-0" href="<?php echo Paginator::previousPageUrl() ?>" tabindex="-1"><?php echo $L->get('Previous'); ?></a>
        </li>
      <?php else : ?>
        <li class="page-item m-0 p-0">
          <span class="page-link m-0" tabindex="-1"><?php echo $L->get('Previous'); ?></span>
        </li>
      <?php endif; ?>

      <!-- Home button -->
      <li class="page-item p-0 m-0">
        <span>Page <?php echo Paginator::currentPage(); ?> sur <?php echo Paginator::numberOfPages(); ?> </span>
      </li>

      <!-- Next button -->
      <?php if (Paginator::showNext()) : ?>
        <li class="page-item m-0 p-0">
          <a class="page-link btn btn-primary m-0" href="<?php echo Paginator::nextPageUrl() ?>"><?php echo $L->get('Next'); ?></a>
        </li>
      <?php else : ?>
        <li class="page-item m-0 p-0">
          <span class="page-link m-0" tabindex="-1"><?php echo $L->get('Next'); ?> </span>
        </li>
      <?php endif; ?>

    </ul>
  </nav>
<?php endif ?>
  </section>
</main>